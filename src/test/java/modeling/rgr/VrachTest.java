package modeling.rgr;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by Victor Novik
 */
public class VrachTest {
    private static Factory factory;
    private static process.Dispatcher dispetcher;
    private static PoliklinikModel model;
    private static Vrach vrach;

    @BeforeClass
    public static void beforeClassTest () {
        factory = new Factory(new PoliklinikGUI());
        dispetcher = new process.Dispatcher();
        model = factory.createModel(dispetcher);
        vrach = new Vrach(model);
    }
    @Test
    public void testGetQueueToVrach() throws Exception {
        Assert.assertNotNull(vrach.getQueueToVrach());
    }

    @Test
    public void testGetDispatcher() throws Exception {
        Assert.assertNotNull(vrach.getDispatcher());
    }

    @Test
    public void testGetGui() throws Exception {
        Assert.assertNotNull(vrach.getGui());
    }
}
