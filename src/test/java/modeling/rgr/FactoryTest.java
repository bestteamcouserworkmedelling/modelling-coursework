package modeling.rgr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import process.Dispatcher;

/**
 * Create by Aleksandr Trykashnyi.
 */
public class FactoryTest {

    private Factory factory;

    @Before
    public void beforeTest() {
        factory = new Factory(new PoliklinikGUI());
    }

    @Test
    public void testCreateModel() {
        PoliklinikModel model = factory.createModel(new Dispatcher());
        Assert.assertNotNull(model);
    }
}
