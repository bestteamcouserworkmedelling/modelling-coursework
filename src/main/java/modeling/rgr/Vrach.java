package modeling.rgr;

import java.util.Random;

import process.Actor;
import process.Dispatcher;
import process.DispatcherFinishException;
import process.IWaitCondition;
import process.QueueForTransactions;
import rnd.Randomable;

public class Vrach extends Actor {
	
	private QueueForTransactions queueToVrach;
	private Dispatcher dispatcher;
	private PoliklinikGUI gui;
	private int workTime;
	private Randomable timeOfOsmotr;
	private double smenaVracha;
	private double povtorKyrs;
	private double verAnalizy;
	private PoliklinikModel model;
	
	public Vrach(PoliklinikModel model){
		this.dispatcher=model.getDispatcher();
		this.gui=model.getGui();
		this.workTime=this.gui.getChooseDataTimeModeling().getInt();
		this.timeOfOsmotr=this.gui.getChooseRandomOsmotrTime().getRandom();
		this.smenaVracha = this.gui.getChooseDataSmDortor().getDouble();
		this.povtorKyrs = this.gui.getChooseDataPovtKuts().getDouble();
		this.verAnalizy = this.gui.getChooseDataAmbKurs().getDouble();
		this.model = model;		
	}

	public QueueForTransactions getQueueToVrach(){ 
		if(queueToVrach == null){
			this.queueToVrach = new QueueForTransactions();
			queueToVrach.setDispatcher(dispatcher);
			//queueToVrach.setPainter(this.getGui().getDiagramOcheredBolnuh().getPainter());			
		}
		return this.queueToVrach;
	}
	
	
	public Dispatcher getDispatcher() {
		return dispatcher;
	}

	public PoliklinikGUI getGui() {
		return gui;
	}

	private boolean isQEmpty(){
		if(getQueueToVrach().size() == 0){
			return true;
		}
		return false;
	}
	
	@Override
	protected void rule() {
		IWaitCondition ISREADY = new IWaitCondition() {
			public boolean testCondition() {
				return !isQEmpty();
			}
			public String toString(){
				return Vrach.this.getNameForProtocol() + "���� ��������";
			}
		};
		while(this.getDispatcher().getCurrentTime()<=this.workTime){
			
			if(isQEmpty()){
				this.model.getPoliklinik().getQVrachiBezrabotu().add(this);
			}
			
			try {
				
				waitForCondition(ISREADY);
				this.model.getPoliklinik().getQVrachiBezrabotu().remove(this);
			} catch (DispatcherFinishException e) {
				return;
			}
			Pacient p=(Pacient)this.getQueueToVrach().removeFirst();
			this.model.getPoliklinik().getqObschayToVrachi().remove(p);
			holdForTime(this.timeOfOsmotr.next());
			this.queueToVrach.remove(p);
			Random rand = new Random(System.currentTimeMillis());			
			double rnd =  rand.nextDouble();
			if(rnd <= this.smenaVracha){
				p.setDiagnoz(1);
				continue;
			}
			rnd = rand.nextDouble();
			if(rnd<=this.verAnalizy){
					p.setDiagnoz(2);			
				continue;
			}
			rnd = rand.nextDouble();
			if(rnd<=this.povtorKyrs && !p.getAtFirst()){
			//if(rnd<=this.povtorKyrs){
				p.setDiagnoz(3);
				continue;
			} else if (!p.getAtFirst()){
				p.setDiagnoz(4);
				continue;
			}
			p.setDiagnoz(5);
		}

	}

}
