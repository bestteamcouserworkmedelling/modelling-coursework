package modeling.rgr;

import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Dimension;
import javax.swing.JSplitPane;
import java.awt.Rectangle;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import widgets.ChooseRandom;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import widgets.ChooseData;
import javax.swing.JTabbedPane;
import widgets.Diagram;
import javax.swing.JLabel;
import java.awt.event.KeyEvent;
import javax.swing.SwingConstants;

import qusystem.IModelFactory;
import rnd.Linear;
import rnd.Negexp;
import rnd.Erlang;
import rnd.Discret;
import rnd.Norm;
import rnd.Triangular;
import javax.swing.JButton;

import process.Dispatcher;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import widgets.experiments.ExperimentControl;
import widgets.regres.RegresAnaliser;
import widgets.trans.TransMonitorView;

public class PoliklinikGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JSplitPane jSplitPane = null;
	private JPanel jPanelleft = null;
	private JPanel jPanelright = null;
	private ChooseRandom chooseRandomIntrlTime = null;
	private ChooseRandom chooseRandomOsmotrTime = null;
	private ChooseRandom chooseRandomTimeOfLecheniy = null;
	private ChooseRandom chooseRandomTimeReaserch = null;
	private ChooseData chooseDataDoctorCount = null;
	private ChooseData chooseDataPovtKuts = null;
	private ChooseData chooseDataAmbKurs = null;
	private ChooseData chooseDataSmDortor = null;
	private ChooseData chooseDataTimeModeling = null;
	private JTabbedPane jTabbedPane = null;
	private JPanel jPanelTest = null;
	private JPanel jPanelStatistica = null;
	private Diagram diagramOcheredBolnuh = null;
	private Diagram diagramPacientuSdAnalizu = null;
	private Diagram diagramVrachiBezRabotu = null;
	private Diagram diagramZavershiloLechene = null;
	private JLabel jLabel = null;
	private JPanel jPanelContenuer = null;
	private JPanel jPanelStart = null;  //  @jve:decl-index=0:visual-constraint="751,65"
	private JButton jButton = null;
	private JTabbedPane jTabbedPane1 = null;
	private JPanel jPanelTestStatistic1 = null;
	private JPanel jPanelTestStatistic2 = null;
	private Diagram diagramStatisticQueue = null;
	private JScrollPane jScrollPane = null;
	private JTextArea jTextAreaStatisticQueue = null;
	private Diagram diagramStatisticTime = null;
	private JScrollPane jScrollPane1 = null;
	private JTextArea jTextAreaStatisticTime = null;
	private JPanel jPanelAnalizTab = null;
	private ExperimentControl experimentControl = null;
	private Diagram diagramAnaliz = null;
	private RegresAnaliser regresAnaliser = null;
    private JPanel jPanelPP;
    private TransMonitorView transMonitorView;
    private Diagram ppDiagram;

    /**
	 * This method initializes jSplitPane	
	 * 	
	 * @return javax.swing.JSplitPane	
	 */
	private JSplitPane getJSplitPane() {
		if (jSplitPane == null) {
			jSplitPane = new JSplitPane();
			jSplitPane.setName("jSplitPane");
			jSplitPane.setDividerLocation(200);
			jSplitPane.setRightComponent(getJPanelright());
			jSplitPane.setLeftComponent(getJPanelleft());
		}
		return jSplitPane;
	}

	/**
	 * This method initializes jPanelleft	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelleft() {
		if (jPanelleft == null) {
			jLabel = new JLabel();
			jLabel.setText("��������� �������");
			jLabel.setHorizontalAlignment(SwingConstants.CENTER);
			jLabel.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(10);
			gridLayout.setColumns(1);
			jPanelleft = new JPanel();
			jPanelleft.setLayout(gridLayout);
			jPanelleft.add(jLabel, null);
			jPanelleft.add(getChooseDataTimeModeling(), null);
			jPanelleft.add(getChooseRandomTimeOfLecheniy(), null);
			jPanelleft.add(getChooseRandomOsmotrTime(), null);
			jPanelleft.add(getChooseRandomIntrlTime(), null);
			jPanelleft.add(getChooseRandomTimeReaserch(), null);
			jPanelleft.add(getChooseDataDoctorCount(), null);
			jPanelleft.add(getChooseDataSmDortor(), null);
			jPanelleft.add(getChooseDataPovtKuts(), null);
			jPanelleft.add(getChooseDataAmbKurs(), null);
		}
		return jPanelleft;
	}

	/**
	 * This method initializes jPanelright	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelright() {
		if (jPanelright == null) {
			jPanelright = new JPanel();
			jPanelright.setLayout(new BorderLayout());
			jPanelright.add(getJTabbedPane(),BorderLayout.CENTER);
			jPanelright.add(getJButton(), BorderLayout.SOUTH);
			
		}
		return jPanelright;
	}

	/**
	 * This method initializes chooseRandomIntrlTime
	 * <br><b>����� ����� ����������</b>	
	 * 	
	 * @return widgets.ChooseRandom	
	 */
	protected ChooseRandom getChooseRandomIntrlTime() {
		if (chooseRandomIntrlTime == null) {
			Norm norm1 = new Norm();
			norm1.setM(3.0D);
			norm1.setSigma(0.5D);
			chooseRandomIntrlTime = new ChooseRandom();
			chooseRandomIntrlTime.setTitle("����� ����� ����������");
			chooseRandomIntrlTime.setPreferredSize(new Dimension(342, 87));
			chooseRandomIntrlTime.setToolTipText("����� �������");
			chooseRandomIntrlTime.setRandom(norm1);
			chooseRandomIntrlTime.setMinimumSize(new Dimension(199, 87));
		}
		return chooseRandomIntrlTime;
	}

	/**
	 * This method initializes chooseRandomOsmotrTime	
	 * 	
	 * @return widgets.ChooseRandom	
	 */
	protected ChooseRandom getChooseRandomOsmotrTime() {
		if (chooseRandomOsmotrTime == null) {
			chooseRandomOsmotrTime = new ChooseRandom();
			chooseRandomOsmotrTime.setTitle("����� �������");
			chooseRandomOsmotrTime.setRandom(new Triangular(2, 3, 5));
		}
		return chooseRandomOsmotrTime;
	}

	/**
	 * This method initializes chooseRandomTimeOfLecheniy	
	 * 	
	 * @return widgets.ChooseRandom	
	 */
	protected ChooseRandom getChooseRandomTimeOfLecheniy() {
		if (chooseRandomTimeOfLecheniy == null) {
			Norm norm = new Norm();
			norm.setM(20.0D);
			norm.setSigma(0.5D);
			chooseRandomTimeOfLecheniy = new ChooseRandom();
			chooseRandomTimeOfLecheniy.setTitle("����� �������");
			chooseRandomTimeOfLecheniy.setRandom(norm);
		}
		return chooseRandomTimeOfLecheniy;
	}

	/**
	 * This method initializes chooseRandomTimeReaserch	
	 * 	
	 * @return widgets.ChooseRandom	
	 */
	protected ChooseRandom getChooseRandomTimeReaserch() {
		if (chooseRandomTimeReaserch == null) {
			Norm norm2 = new Norm();
			norm2.setM(1.0D);
			norm2.setSigma(0.5D);
			chooseRandomTimeReaserch = new ChooseRandom();
			chooseRandomTimeReaserch.setTitle("����� �����������");
			chooseRandomTimeReaserch.setRandom(norm2);
		}
		return chooseRandomTimeReaserch;
	}

	/**
	 * This method initializes chooseDataDoctorCount	
	 * 	
	 * @return widgets.ChooseData	
	 */
	protected ChooseData getChooseDataDoctorCount() {
		if (chooseDataDoctorCount == null) {
			chooseDataDoctorCount = new ChooseData();
			chooseDataDoctorCount.setTitle("���������� ������");
			chooseDataDoctorCount.setText("20");
			getDiagramVrachiBezRabotu().setVerticalMaxText(getChooseDataDoctorCount().getText());
			chooseDataDoctorCount.addCaretListener(new javax.swing.event.CaretListener() {
				public void caretUpdate(javax.swing.event.CaretEvent e) {
					getDiagramVrachiBezRabotu().setVerticalMaxText(getChooseDataDoctorCount().getText());
				}
			});
		}
		return chooseDataDoctorCount;
	}

	/**
	 * This method initializes chooseDataPovtKuts	
	 * 	
	 * @return widgets.ChooseData	
	 */
	protected ChooseData getChooseDataPovtKuts() {
		if (chooseDataPovtKuts == null) {
			chooseDataPovtKuts = new ChooseData();
			chooseDataPovtKuts.setTitle("����������� ���������� �����");
			chooseDataPovtKuts.setText("0.3");
		}
		return chooseDataPovtKuts;
	}

	/**
	 * This method initializes chooseDataAmbKurs	
	 * 	
	 * @return widgets.ChooseData	
	 */
	protected ChooseData getChooseDataAmbKurs() {
		if (chooseDataAmbKurs == null) {
			chooseDataAmbKurs = new ChooseData();
			chooseDataAmbKurs.setTitle("����������� ������������� �����");
			chooseDataAmbKurs.setText("0.7");
		}
		return chooseDataAmbKurs;
	}

	/**
	 * This method initializes chooseDataSmDortor	
	 * 	
	 * @return widgets.ChooseData	
	 */
	protected ChooseData getChooseDataSmDortor() {
		if (chooseDataSmDortor == null) {
			chooseDataSmDortor = new ChooseData();
			chooseDataSmDortor.setTitle("����������� ����� �����");
			chooseDataSmDortor.setText("0.2");
		}
		return chooseDataSmDortor;
	}

	/**
	 * This method initializes chooseDataTimeModeling	
	 * 	
	 * @return widgets.ChooseData	
	 */
	protected ChooseData getChooseDataTimeModeling() {
		if (chooseDataTimeModeling == null) {
			chooseDataTimeModeling = new ChooseData();
			chooseDataTimeModeling.setTitle("����� �������������");
			chooseDataTimeModeling.setInt(100);
			chooseDataTimeModeling.setText("1000");
			getDiagramOcheredBolnuh().setHorizontalMaxText(getChooseDataTimeModeling().getText());
			getDiagramPacientuSdAnalizu().setHorizontalMaxText(getChooseDataTimeModeling().getText());
			getDiagramVrachiBezRabotu().setHorizontalMaxText(getChooseDataTimeModeling().getText());
			getDiagramZavershiloLechene().setHorizontalMaxText(getChooseDataTimeModeling().getText());
			chooseDataTimeModeling.addCaretListener(new javax.swing.event.CaretListener() {
				public void caretUpdate(javax.swing.event.CaretEvent e) {
					getDiagramOcheredBolnuh().setHorizontalMaxText(getChooseDataTimeModeling().getText());
					getDiagramPacientuSdAnalizu().setHorizontalMaxText(getChooseDataTimeModeling().getText());
					getDiagramVrachiBezRabotu().setHorizontalMaxText(getChooseDataTimeModeling().getText());
					getDiagramZavershiloLechene().setHorizontalMaxText(getChooseDataTimeModeling().getText());
				}
			});
		}
		return chooseDataTimeModeling;
	}

    public TransMonitorView getTransMonitorView() {
        if(this.transMonitorView == null) {
            this.transMonitorView = new TransMonitorView();
            this.transMonitorView.setDiagram(getPpDiagram());
            this.transMonitorView.setFactory(new Factory(PoliklinikGUI.this));
        }
        return this.transMonitorView;
    }

	/**
	 * This method initializes jTabbedPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getJTabbedPane() {
		if (jTabbedPane == null) {
			jTabbedPane = new JTabbedPane();
			jTabbedPane.setName("jTabbedPane");
			jTabbedPane.setToolTipText("");
			jTabbedPane.addTab("���� ������", null, getJPanelTest(), null);
			jTabbedPane.addTab("����������", null, getJPanelStatistica(), null);
			jTabbedPane.addTab("������", null, getJPanelAnalizTab(), null);
            jTabbedPane.addTab("���������� ��������", null, getJPanelPP(), null);
		}
		return jTabbedPane;
	}

	/**
	 * This method initializes jPanelTest	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	protected JPanel getJPanelTest() {
		if (jPanelTest == null) {
			jPanelTest = new JPanel();
			jPanelTest.setLayout(new BorderLayout());
			jPanelTest.add(getJPanelContenuer(), BorderLayout.CENTER);
			jPanelTest.add(getJPanelStart(), BorderLayout.SOUTH);
		}
		return jPanelTest;
	}

	/**
	 * This method initializes jPanelStatistica	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelStatistica() {
		if (jPanelStatistica == null) {
			jPanelStatistica = new JPanel();
			jPanelStatistica.setLayout(new BorderLayout());
			jPanelStatistica.add(getJTabbedPane1(), BorderLayout.CENTER);
		}
		return jPanelStatistica;
	}

    public Diagram getPpDiagram() {
        if(this.ppDiagram == null) {
            this.ppDiagram = new Diagram();
            this.ppDiagram.setPainterColor(Color.red);
        }
        return this.ppDiagram;
    }


    private JPanel getJPanelPP() {
        if (jPanelPP == null) {
            GridLayout gridLayout1 = new GridLayout();
            gridLayout1.setRows(2);
            gridLayout1.setColumns(1);
            jPanelPP = new JPanel();
            jPanelPP.setLayout(gridLayout1);
            jPanelPP.add(getTransMonitorView(), null);
            jPanelPP.add(getPpDiagram(), null);
        }
        return jPanelPP;
    }

	/**
	 * This method initializes diagramOcheredBolnuh	
	 * 	
	 * @return widgets.Diagram	
	 */
	protected Diagram getDiagramOcheredBolnuh() {
		if (diagramOcheredBolnuh == null) {
			diagramOcheredBolnuh = new Diagram();
			diagramOcheredBolnuh.setTitleText("������� �������");
			diagramOcheredBolnuh.setHorizontalMaxText("10");
			diagramOcheredBolnuh.setVerticalMaxText("10");
			diagramOcheredBolnuh.setName("DiagramOcheredBolnuh");
		}
		return diagramOcheredBolnuh;
	}

	/**
	 * This method initializes diagramPacientuSdAnalizu	
	 * 	
	 * @return widgets.Diagram	
	 */
	protected Diagram getDiagramPacientuSdAnalizu() {
		if (diagramPacientuSdAnalizu == null) {
			diagramPacientuSdAnalizu = new Diagram();
			diagramPacientuSdAnalizu.setTitleText("�������� ������� �������");
			diagramPacientuSdAnalizu.setVerticalMaxText("10");
			diagramPacientuSdAnalizu.setPainterColor(Color.magenta);
			diagramPacientuSdAnalizu.setName("DiagramAnaliz");
		}
		return diagramPacientuSdAnalizu;
	}

	/**
	 * This method initializes diagramVrachiBezRabotu	
	 * 	
	 * @return widgets.Diagram	
	 */
	protected Diagram getDiagramVrachiBezRabotu() {
		if (diagramVrachiBezRabotu == null) {
			diagramVrachiBezRabotu = new Diagram();
			diagramVrachiBezRabotu.setTitleText("����� ��� ������");
			diagramVrachiBezRabotu.setName("DiagramVrachi");
		}
		return diagramVrachiBezRabotu;
	}

	/**
	 * This method initializes diagramZavershiloLechene	
	 * 	
	 * @return widgets.Diagram	
	 */
	protected Diagram getDiagramZavershiloLechene() {
		if (diagramZavershiloLechene == null) {
			diagramZavershiloLechene = new Diagram();
			diagramZavershiloLechene.setTitleText("���������� ��������� ����������� �������");
			diagramZavershiloLechene.setVerticalMaxText("100");
			diagramZavershiloLechene.setName("DiagramCntPoc");
		}
		return diagramZavershiloLechene;
	}

	/**
	 * This method initializes jPanelContenuer	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelContenuer() {
		if (jPanelContenuer == null) {
			GridLayout gridLayout1 = new GridLayout();
			gridLayout1.setRows(4);
			gridLayout1.setColumns(1);
			jPanelContenuer = new JPanel();
			jPanelContenuer.setLayout(gridLayout1);
			jPanelContenuer.add(getDiagramOcheredBolnuh(), null);
			jPanelContenuer.add(getDiagramPacientuSdAnalizu(), null);
			jPanelContenuer.add(getDiagramVrachiBezRabotu(), null);
			jPanelContenuer.add(getDiagramZavershiloLechene(), null);
		}
		return jPanelContenuer;
	}

	/**
	 * This method initializes jPanelStart	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelStart() {
		if (jPanelStart == null) {
			jPanelStart = new JPanel();
			jPanelStart.setLayout(new BorderLayout());
			jPanelStart.setSize(new Dimension(90, 117));
		}
		return jPanelStart;
	}

	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setText("�����");
			jButton.setName("jButton");
			jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Dispatcher d = new Dispatcher();
					Factory modelFactory = new Factory(PoliklinikGUI.this);
					PoliklinikModel pmodel=modelFactory.createModel(d);
					if(getJPanelTest().isShowing()){
						startTest(pmodel, d);
					}
					if(getJPanelStatistica().isShowing()){
						startStatistic(pmodel, d);
					}
				}
			});
		}
		return jButton;
	}

	/**
	 * This method initializes jTabbedPane1	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getJTabbedPane1() {
		if (jTabbedPane1 == null) {
			jTabbedPane1 = new JTabbedPane();
			jTabbedPane1.addTab("����� �������", null, getJPanelTestStatistic1(), null);
			jTabbedPane1.addTab("����� ��������", null, getJPanelTestStatistic2(), null);
		}
		return jTabbedPane1;
	}

	/**
	 * This method initializes jPanelTestStatistic1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelTestStatistic1() {
		if (jPanelTestStatistic1 == null) {
			GridLayout gridLayout2 = new GridLayout();
			gridLayout2.setRows(2);
			gridLayout2.setColumns(1);
			jPanelTestStatistic1 = new JPanel();
			jPanelTestStatistic1.setLayout(gridLayout2);
			jPanelTestStatistic1.add(getDiagramStatisticQueue(), null);
			jPanelTestStatistic1.add(getJScrollPane(), null);
		}
		return jPanelTestStatistic1;
	}

	/**
	 * This method initializes jPanelTestStatistic2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelTestStatistic2() {
		if (jPanelTestStatistic2 == null) {
			GridLayout gridLayout3 = new GridLayout();
			gridLayout3.setRows(2);
			gridLayout3.setColumns(1);
			jPanelTestStatistic2 = new JPanel();
			jPanelTestStatistic2.setLayout(gridLayout3);
			jPanelTestStatistic2.add(getDiagramStatisticTime(), null);
			jPanelTestStatistic2.add(getJScrollPane1(), null);
		}
		return jPanelTestStatistic2;
	}

	/**
	 * This method initializes diagramStatisticQueue	
	 * 	
	 * @return widgets.Diagram	
	 */
	protected Diagram getDiagramStatisticQueue() {
		if (diagramStatisticQueue == null) {
			diagramStatisticQueue = new Diagram();
			diagramStatisticQueue.setTitleText("����� ������� � ������");
			diagramStatisticQueue.setHorizontalMaxText("20");
		}
		return diagramStatisticQueue;
	}

	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJTextAreaStatisticQueue());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes jTextAreaStatisticQueue	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getJTextAreaStatisticQueue() {
		if (jTextAreaStatisticQueue == null) {
			jTextAreaStatisticQueue = new JTextArea();
		}
		return jTextAreaStatisticQueue;
	}

	/**
	 * This method initializes diagramStatisticTime	
	 * 	
	 * @return widgets.Diagram	
	 */
	protected Diagram getDiagramStatisticTime() {
		if (diagramStatisticTime == null) {
			diagramStatisticTime = new Diagram();
			diagramStatisticTime.setTitleText("����� ����� ��������");
		}
		return diagramStatisticTime;
	}

	/**
	 * This method initializes jScrollPane1	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane1() {
		if (jScrollPane1 == null) {
			jScrollPane1 = new JScrollPane();
			jScrollPane1.setViewportView(getJTextAreaStatisticTime());
		}
		return jScrollPane1;
	}

	/**
	 * This method initializes jTextAreaStatisticTime	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getJTextAreaStatisticTime() {
		if (jTextAreaStatisticTime == null) {
			jTextAreaStatisticTime = new JTextArea();
		}
		return jTextAreaStatisticTime;
	}

	/**
	 * This method initializes jPanelAnalizTab	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	protected JPanel getJPanelAnalizTab() {
		if (jPanelAnalizTab == null) {
			GridLayout gridLayout4 = new GridLayout();
			gridLayout4.setRows(3);
			gridLayout4.setColumns(1);
			jPanelAnalizTab = new JPanel();
			jPanelAnalizTab.setLayout(gridLayout4);
			jPanelAnalizTab.add(getExperimentControl(), null);
			jPanelAnalizTab.add(getRegresAnaliser(), null);
			jPanelAnalizTab.add(getDiagramAnaliz(), null);
			
		}
		return jPanelAnalizTab;
	}

	/**
	 * This method initializes experimentControl	
	 * 	
	 * @return widgets.experiments.ExperimentControl	
	 */
	private ExperimentControl getExperimentControl() {
		if (experimentControl == null) {
			experimentControl = new ExperimentControl();
			experimentControl.setTextFactors("10 20 30");
			experimentControl.setTextNExpr("3");
			getExperimentControl().setDiagram(getDiagramAnaliz());
			IModelFactory afactory = new Factory(PoliklinikGUI.this);
			getExperimentControl().setFactory(afactory);
		}
		return experimentControl;
	}

	/**
	 * This method initializes diagramAnaliz	
	 * 	
	 * @return widgets.Diagram	
	 */
	private Diagram getDiagramAnaliz() {
		if (diagramAnaliz == null) {
			diagramAnaliz = new Diagram();
			diagramAnaliz.setTitleText("����������� ������� �� ���������� ������");
			diagramAnaliz.setVerticalMaxText("50");
			diagramAnaliz.setHorizontalMaxText("50");
		}
		return diagramAnaliz;
	}

	/**
	 * This method initializes regresAnaliser	
	 * 	
	 * @return widgets.regres.RegresAnaliser	
	 */
	private RegresAnaliser getRegresAnaliser() {
		if (regresAnaliser == null) {
			regresAnaliser = new RegresAnaliser();
			regresAnaliser.setDiagram(getDiagramAnaliz());
			regresAnaliser.setIRegresable(getExperimentControl());
			
		}
		return regresAnaliser;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				PoliklinikGUI thisClass = new PoliklinikGUI();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public PoliklinikGUI() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(696, 483);
		this.setContentPane(getJContentPane());
		this.setTitle("�������������� ������ �����������");
		
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new CardLayout());
			jContentPane.add(getJSplitPane(), getJSplitPane().getName());
		}
		return jContentPane;
	}
	
	private void startTest(PoliklinikModel model,Dispatcher d){
		model.initTestDiagrams();
		d.start();
		getJButton().setEnabled(false);
		try{
			d.getThread().join();
		}catch(InterruptedException exp){
			exp.printStackTrace();
		}
		getJButton().setEnabled(true);
	}
	
	private void startStatistic(PoliklinikModel model, Dispatcher d){
		model.initStatisticDiagrams();
		d.start();
		getJButton().setEnabled(false);
		try{
			d.getThread().join();
		}catch(InterruptedException exp){
			exp.printStackTrace();
		}
		getJTextAreaStatisticQueue().setText(model.getDiscretHistoQToVrachi().toString());
		getJTextAreaStatisticQueue().select(0, 0);
		model.getDiscretHistoQToVrachi().showRelFrec(getDiagramStatisticQueue());
		getJTextAreaStatisticTime().setText(model.getHistoTime().toString());
		getJTextAreaStatisticTime().select(0, 0);
		model.getHistoTime().showRelFrec(getDiagramStatisticTime());
		getJButton().setEnabled(true);
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
