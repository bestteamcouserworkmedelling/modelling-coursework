package modeling.rgr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import process.Dispatcher;
import process.QueueForTransactions;

import java.util.ArrayList;

/**
 * Create by Aleksandr Trykashnyi.
 */
public class PoliklinikTest {

    private Poliklinik poliklinik;
    private static PoliklinikModel  model;

    @BeforeClass
    public static void init() {
        Factory factory = new Factory(new PoliklinikGUI());
        model = factory.createModel(new Dispatcher());
    }

    @Before
    public void beforeTest() {
        poliklinik = new Poliklinik(model);
    }

    @Test
    public void testGetModelingTime() throws Exception {
        int modelingTime = poliklinik.getModelingTime();
        Assert.assertNotNull(modelingTime);
    }

    @Test
    public void testGetDispatcher() throws Exception {
        Dispatcher dispatcher = poliklinik.getDispatcher();
        Assert.assertNotNull(dispatcher);
    }

    @Test
    public void testGetGui() throws Exception {
        PoliklinikGUI gui = poliklinik.getGui();
        Assert.assertNotNull(gui);
    }

    @Test
    public void testRule() throws Exception {

    }

    @Test
    public void testGetQAnalizy() throws Exception {
        QueueForTransactions qAnalizy = poliklinik.getQAnalizy();
        Assert.assertNotNull(qAnalizy);
    }

    @Test
    public void testGetQLechenie() throws Exception {
        QueueForTransactions qLechenie = poliklinik.getQLechenie();
        Assert.assertNotNull(qLechenie);
    }

    @Test
    public void testGetQZdorovue() throws Exception {
        QueueForTransactions qZdorovue = poliklinik.getQZdorovue();
        Assert.assertNotNull(qZdorovue);
    }

    @Test
    public void testGetQVrachiBezrabotu() throws Exception {
        QueueForTransactions qVrachiBezrabotu = poliklinik.getQVrachiBezrabotu();
        Assert.assertNotNull(qVrachiBezrabotu);
    }

    @Test
    public void testGetqObschayToVrachi() throws Exception {
        QueueForTransactions queueForTransactions = poliklinik.getqObschayToVrachi();
        Assert.assertNotNull(queueForTransactions);
    }

    @Test
    public void testGetCountVrachi() throws Exception {
        int countVrachi = poliklinik.getCountVrachi();
        Assert.assertNotNull(countVrachi);
    }
}
