package modeling.rgr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sun.rmi.server.Dispatcher;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.server.RemoteCall;

/**
 * Created by victor on 4/6/14.
 */
public class PoliklinikModelTest {
    private static Factory factory;
    private static process.Dispatcher dispetcher;
    private static PoliklinikModel model;

    @BeforeClass
    public static void beforeClassTest () {
        factory = new Factory(new PoliklinikGUI());
        dispetcher = new process.Dispatcher();
        model = factory.createModel(dispetcher);
    }

    @Test
    public void testGetDispatcher() throws Exception {
        Assert.assertNotNull(model.getDispatcher());
    }

    @Test
    public void testGetPoliklinik() throws Exception {
        Assert.assertNotNull(model.getPoliklinik());
    }

    @Test
    public void testGetGui() throws Exception {
        Assert.assertTrue(model.getGui() instanceof PoliklinikGUI);
    }

    @Test
    public void testGetDiscretHistoQToVrachi() throws Exception {
        Assert.assertNotNull(model.getDiscretHistoQToVrachi());
    }

    @Test
    public void testGetHistoTime() throws Exception {
        Assert.assertNotNull(model.getHistoTime());
    }

    @Test
    public void testGetResultOfExperiment() throws Exception {
        Assert.assertNotNull(model.getResultOfExperiment());
    }
}
