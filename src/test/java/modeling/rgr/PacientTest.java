package modeling.rgr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import process.Dispatcher;

import static org.junit.Assert.assertNotNull;

/**
 * Create by Aleksandr Trykashnyi.
 */
public class PacientTest {

    private Pacient pacient;
    private static PoliklinikModel model;

    @BeforeClass
    public static void init() {
        Factory factory = new Factory(new PoliklinikGUI());
        model = factory.createModel(new Dispatcher());
    }

    @Before
    public void beforeTest() {
        pacient = new Pacient(model);
    }

    @Test
    public void testGetVrach() throws Exception {
        pacient.setVrach(new Vrach(model));
        Vrach vrach = pacient.getVrach();
        assertNotNull(vrach);
    }

    @Test
    public void testGetAtFirst() throws Exception {
        boolean atFirst = pacient.getAtFirst();
        Assert.assertNotNull(atFirst);
    }

    @Test
    public void testGetPredVrach() throws Exception {
        pacient.setPredVrach(new Vrach(model));
        Vrach predVrach = pacient.getPredVrach();
        Assert.assertNotNull(predVrach);
    }

    @Test
    public void testRule() throws Exception {

    }

    @Test
    public void testGetDiagnoz() throws Exception {
        int diagnoz = pacient.getDiagnoz();
        Assert.assertNotNull(diagnoz);
    }
}
