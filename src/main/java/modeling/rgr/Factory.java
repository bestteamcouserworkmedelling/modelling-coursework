package modeling.rgr;

import process.Dispatcher;
import qusystem.IModelFactory;

public class Factory implements IModelFactory {
	private PoliklinikGUI gui;
	
	public Factory(PoliklinikGUI gui){
		this.gui = gui;
	}

	public PoliklinikModel createModel(Dispatcher dispatcher) {
		return new PoliklinikModel(dispatcher, this.gui);
	}

}
