package modeling.rgr;

import java.awt.Color;

import process.Dispatcher;
import process.QueueForTransactions;
import stat.DiscretHisto;
import stat.Histo;
import widgets.experiments.IExperimentable;
import widgets.trans.ITransProcesable;

public class PoliklinikModel implements IExperimentable, ITransProcesable{
    public void initForTrans(double v) {
        getGui().getChooseDataTimeModeling().setText(String.valueOf(v));
    }

    public void resetTransAccum() {
        getPoliklinik().getqObschayToVrachi().resetAccum();
    }

    public double getTransResult() {
        return getDiscretHistoQToVrachi().getAverage();
    }

    private Dispatcher dispatcher;
	private PoliklinikGUI gui;
	private Poliklinik poliklinik;
	private DiscretHisto discretHistoQToVrachi;
	private Histo histoTime;
	
	
	
	public PoliklinikModel(Dispatcher d, PoliklinikGUI gui){
		super();
		if (d == null || gui == null) {
			System.out.println("�� ��������� ���������� ��� GUI ��� Model");
			System.out.println("�������� ������ ���������");
			System.exit(0);
		}
		this.dispatcher=d;
		this.gui=gui;
		//�������� ������ �� ���������� ������ ����������
		componentsToStartList();
		
	}
	

	public Dispatcher getDispatcher() {
		return dispatcher;
	}

	public Poliklinik getPoliklinik() {
		return poliklinik;
	}

	public PoliklinikGUI getGui() {
		return gui;
	}

	private void componentsToStartList() {
		this.poliklinik=new Poliklinik(this);
		this.dispatcher.addStartingActor(this.poliklinik);
	}


	public DiscretHisto getDiscretHistoQToVrachi() {
		if(discretHistoQToVrachi==null){
			discretHistoQToVrachi= new DiscretHisto();
		}
		return discretHistoQToVrachi;
	}


	public Histo getHistoTime() {
		if(histoTime==null){
			histoTime= new Histo();
		}
		return histoTime;
	}
	
	public void initTestDiagrams(){
		this.gui.getDiagramOcheredBolnuh().clear();
		this.gui.getDiagramPacientuSdAnalizu().clear();
		this.getPoliklinik().getQAnalizy().setPainter(this.gui.getDiagramPacientuSdAnalizu().getPainter());
		this.gui.getDiagramVrachiBezRabotu().clear();
		this.getPoliklinik().getQVrachiBezrabotu().setPainter(this.gui.getDiagramVrachiBezRabotu().getPainter());
		this.getPoliklinik().getQVrachiBezrabotu().getPainter().setColor(new Color(0, 0 , 255));
		this.getPoliklinik().getQVrachiBezrabotu().getPainter().placeToXY(0, 0);
		this.gui.getDiagramZavershiloLechene().clear();
		this.getPoliklinik().getQZdorovue().setPainter(this.gui.getDiagramZavershiloLechene().getPainter());
		this.getPoliklinik().getQZdorovue().getPainter().setColor(new Color(0, 0 , 255));	
	}
	
	public void initStatisticDiagrams(){
		this.gui.getDiagramStatisticQueue().clear();
		this.getPoliklinik().getqObschayToVrachi().setDiscretHisto(this.getDiscretHistoQToVrachi());
		this.gui.getDiagramStatisticTime().clear();
	}


	public void initForExperiment(double factor) {
		this.getPoliklinik().setCountVrachi((int)factor);
		//this.getDispatcher().setProtocolFileName("");
		
	}


	public double getResultOfExperiment() {
		return this.getDiscretHistoQToVrachi().getAverage();
	}

	

}
