package modeling.rgr;


import java.util.Random;

import process.Actor;
import process.DispatcherFinishException;
import process.IWaitCondition;
import rnd.Randomable;

public class Pacient extends Actor {
	
	private Vrach vrach = null;
	private Vrach predVrach = null;
	private PoliklinikModel model;
	private Randomable timeOfAnalizy;
	private Randomable timeOfLechenie;
	private boolean atFirst = true;
	private int diagnoz = 0;
	
	public Pacient(PoliklinikModel model){
		this.model=model;
		this.timeOfAnalizy = this.model.getGui().getChooseRandomTimeReaserch().getRandom();		
		this.timeOfLechenie = this.model.getGui().getChooseRandomTimeOfLecheniy().getRandom();
	}
	
	private void initPacient(){
		Random rnd =new Random(System.currentTimeMillis());
		int rndIndexVrach=rnd.nextInt(this.model.getPoliklinik().getArrayVrachi().size());
		this.vrach=this.model.getPoliklinik().getArrayVrachi().get(rndIndexVrach);
	}
	
	

	public Vrach getVrach() {
		return vrach;
	}

	public void setVrach(Vrach vrach) {
		this.vrach = vrach;
	}
	
	
	
	public void holdTimeForAnalizy(){
		this.model.getPoliklinik().getQAnalizy().add(this);
		double waitTime=this.timeOfAnalizy.next();
		this.model.getHistoTime().add(waitTime);
		holdForTime(waitTime);
		this.model.getPoliklinik().getQAnalizy().remove(this);
		setDiagnoz(0);
		this.vrach.getQueueToVrach().add(this);
		this.model.getPoliklinik().getqObschayToVrachi().add(this);
	}
	
	public void holdForLechenie(){
		this.model.getPoliklinik().getQLechenie().add(this);
		holdForTime(this.timeOfLechenie.next());
		this.model.getPoliklinik().getQLechenie().remove(this);
		setDiagnoz(0);
		this.vrach.getQueueToVrach().add(this);
		this.model.getPoliklinik().getqObschayToVrachi().add(this);
		this.atFirst = false;
	}
	
	public boolean getAtFirst(){
		return this.atFirst;
	}
	
	public void addToZdorovue(){
		this.model.getPoliklinik().getQZdorovue().add(this);
	}
	
	public void setPredVrach(Vrach v){
		this.predVrach = v;
	}
	
	public Vrach getPredVrach(){
		return this.predVrach;
	}
	
	
	
	
	
	@Override
	protected void rule() {
		initPacient();		
		this.vrach.getQueueToVrach().add(this);
		this.model.getPoliklinik().getqObschayToVrachi().add(this);
		double tBegin=this.getDispatcher().getCurrentTime();
		IWaitCondition waitDiagnoz = new IWaitCondition() {

			public boolean testCondition() {				
				return diagnoz > 0;
			}
			
			public String toString(){
				return "������� ���� ����������";
			}
		};
		
		while(true){
			/*
			 * 1 - ����� �����
			 * 2 - ������������ ������������
			 * 3 - ��������� �� �������
			 * 4 - ������
			 */
			try {
				waitForCondition(waitDiagnoz);
			} catch (DispatcherFinishException e) {
				return;
			}
			double tEnd= this.getDispatcher().getCurrentTime();
			this.model.getHistoTime().add(tEnd-tBegin);
			switch(this.diagnoz){
			case 1:
				int rndIndexVrach;
				Vrach v_temp;
				Random rand = new Random(System.currentTimeMillis());
				do{
					rndIndexVrach=rand.nextInt(this.model.getPoliklinik().getArrayVrachi().size());
					v_temp = this.model.getPoliklinik().getArrayVrachi().
							get(rndIndexVrach);
				}while(this.getVrach() == v_temp);
				
				setPredVrach(this.vrach);
				setVrach(v_temp);
				setDiagnoz(0);
				v_temp.getQueueToVrach().add(this);
				this.model.getPoliklinik().getqObschayToVrachi().add(this);
				this.model.getDispatcher().printToProtocol("������ ������ �����");
				break;
			case 2:
				holdTimeForAnalizy();				
				break;
			case 3:
				holdForLechenie();
				break;
			case 4:	
				this.model.getPoliklinik().getQZdorovue().add(this);
				return;
			default:
				this.holdForLechenie();
			}
			/*

			*/
		}
	}
	
	
	
	
	
	public void setDiagnoz(int d){
		this.diagnoz = d;
	}
	
	public int getDiagnoz(){
		return this.diagnoz;
	}

}
