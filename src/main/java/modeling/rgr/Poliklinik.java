package modeling.rgr;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import paint.Painter;
import process.Actor;
import process.Dispatcher;
import process.QueueForTransactions;
import rnd.Randomable;
import widgets.trans.TransProcessQueue;

public class Poliklinik extends Actor {
	
	private Dispatcher dispatcher;
	private int modelingTime;
	private PoliklinikGUI gui;
	private ArrayList<Vrach> arrayVrachi;
	private QueueForTransactions vrachiBezrabotu;
	private PoliklinikModel model;
	private Randomable timeInrvalBwPacient;
	private QueueForTransactions qanalizy;
	private QueueForTransactions qLechinie;
	private QueueForTransactions qZdorovennue;
	private TransProcessQueue qObschayToVrachi;
	private int countVrachi;
	
	public Poliklinik(PoliklinikModel model){
		this.dispatcher=model.getDispatcher();
		this.gui=model.getGui();
		this.modelingTime=this.gui.getChooseDataTimeModeling().getInt();
		this.timeInrvalBwPacient=this.getGui().getChooseRandomIntrlTime().getRandom();
		this.model  = model;
		//initPoliklinik();
	}
	
	private void initPoliklinik(){
		int n = this.getCountVrachi();
		if(!this.getGui().getJPanelAnalizTab().isShowing()){
			n=this.gui.getChooseDataDoctorCount().getInt();	
		}
		arrayVrachi=new ArrayList<Vrach>(n);
		for(int i=0; i<n;i++){
			Vrach vrach=new Vrach(this.model);
			if(this.gui.getJPanelTest().isShowing()){
				Painter painter = new Painter(this.gui.getDiagramOcheredBolnuh());
				Random r = new Random(System.currentTimeMillis());
				int rndR = r.nextInt(255);
				int rndG = r.nextInt(255);
				int rndB = r.nextInt(255);
				painter.setColor(new Color(rndR, rndG, rndB));
				painter.placeToXY(0, 0);
				vrach.getQueueToVrach().setPainter(painter);
			}
			arrayVrachi.add(vrach);
			vrach.setNameForProtocol("����: "+i);
			this.getDispatcher().addStartingActor(vrach);
		}
		/*
		 * ������������� ������� ��������
		 */
		//this.qanalizy = new QueueForTransactions(); 
		//this.qanalizy.setDispatcher(this.dispatcher);
		//this.qanalizy.setPainter(this.gui.getDiagramPacientuSdAnalizu().getPainter());
	}
	
	

	public int getModelingTime() {
		return modelingTime;
	}

	public void setModelingTime(int modelingTime) {
		this.modelingTime = modelingTime;
	}

	public Dispatcher getDispatcher() {
		return dispatcher;
	}

	public PoliklinikGUI getGui() {
		return gui;
	}

	public ArrayList<Vrach> getArrayVrachi() {
		return arrayVrachi;
	}

	@Override
	protected void rule() {
		initPoliklinik();
		int i=1;
		//System.out.print(this.getDispatcher().getCurrentTime());
		while(this.getDispatcher().getCurrentTime() <= this.modelingTime){
			Pacient p=new Pacient(this.model);
			p.setNameForProtocol("�������: " +i);
			this.dispatcher.addStartingActor(p);
			this.holdForTime(this.timeInrvalBwPacient.next());
			i++;
		}

	}
	
	public QueueForTransactions getQAnalizy(){
		if(this.qanalizy==null){
			this.qanalizy = new QueueForTransactions(); 
			this.qanalizy.setDispatcher(this.dispatcher);
		}
		return this.qanalizy;
	}
	
	public QueueForTransactions getQLechenie(){
		if(this.qLechinie == null){
			this.qLechinie = new QueueForTransactions();
			this.qLechinie.setDispatcher(this.dispatcher);
		}
		return this.qLechinie;
	}
	
	public QueueForTransactions getQZdorovue(){
		if(this.qZdorovennue == null){
			this.qZdorovennue = new QueueForTransactions();
			this.qZdorovennue.setDispatcher(this.dispatcher);
			/*this.qZdorovennue.setPainter(this.gui.getDiagramZavershiloLechene().getPainter());
			this.qZdorovennue.getPainter().setColor(new Color(0, 0 , 255));*/
		}
		return this.qZdorovennue;
	}
	
	public QueueForTransactions getQVrachiBezrabotu(){
		if(this.vrachiBezrabotu == null){
			this.vrachiBezrabotu = new QueueForTransactions();
			this.vrachiBezrabotu.setDispatcher(this.dispatcher);
			/*this.vrachiBezrabotu.setPainter(this.gui.getDiagramVrachiBezRabotu().getPainter());
			this.vrachiBezrabotu.getPainter().setColor(new Color(0, 0 , 255));
			this.vrachiBezrabotu.getPainter().placeToXY(0, 0);*/
		}
		return this.vrachiBezrabotu;
	}

	public TransProcessQueue getqObschayToVrachi() {
		if(this.qObschayToVrachi==null){
			this.qObschayToVrachi= new TransProcessQueue();
			this.qObschayToVrachi.setNameForProtocol("����� ������� � ������");
			this.qObschayToVrachi.setDispatcher(this.getDispatcher());
			this.qObschayToVrachi.setDiscretHisto(this.model.getDiscretHistoQToVrachi());
		}
		return qObschayToVrachi;
	}

	public int getCountVrachi() {
		return countVrachi;
	}

	public void setCountVrachi(int countVrachi) {
		this.countVrachi = countVrachi;
	}
	
	

}
